# frozen_string_literal: true

class Building < ApplicationRecord
  has_many :users_buildings, dependent: :destroy
  has_many :users, through: :users_buildings
end
