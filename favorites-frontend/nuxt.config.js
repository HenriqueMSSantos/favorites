const dev = process.env.NODE_ENV === 'development'
const baseUrl = dev
  ? 'http://localhost:3000'
  : 'https://siacad-rs.herokuapp.com'

const PORT = process.env.PORT || 8080

export default {
  // Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
  ssr: false,
  server: {
    port: PORT,
    host: '0.0.0.0',
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    titleTemplate: '%s',
    title: 'favorites-frontend',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [],

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: ['@/plugins/cookieFactory', '@/plugins/axios.js'],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: ['@nuxtjs/auth', '@nuxtjs/axios'],

  axios: {
    progress: true,
    https: !dev,
    proxy: !dev,
  },
  router: {
    middleware: ['auth'],
  },
  auth: {
    redirect: {
      login: '/signin',
      home: '/',
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            url: baseUrl + '/login',
            method: 'post',
            propertyName: 'authorization',
          },
          logout: {
            url: '/logout',
            method: 'delete',
          },
          user: {
            url: '/user',
            method: 'get',
            propertyName: false,
          },
        },
      },
    },
  },

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        light: {
          primary: '#117AE4', // verde I
        },
      },
    },
  },

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},
}
