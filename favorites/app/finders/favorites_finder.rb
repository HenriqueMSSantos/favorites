# frozen_string_literal: true

class FavoritesFinder < ApplicationFinder
  def find(id)
    @users_buildings = UsersBuilding.where(user_id: id)

    data
  end

  def data
    @buildings = []
    p @users_buildings[0]

    @users_buildings.each do |t|
      @buildings << {
        relation_id: t.id,
        id: t.building.id,
        title: t.building.title,
        city: t.building.city,
        area: t.building.area,
        state: t.building.state,
        min_area: t.building.min_area,
        max_area: t.building.max_area,
        image_url: t.building.image_url
      }
    end

    @buildings
  end
end
