# frozen_string_literal: true

require 'test_helper'

class UsersBuildingTest < ActiveSupport::TestCase
  test 'fixture is valid' do
    @objective = users_buildings(:one)
    assert @objective.valid?
  end
end
