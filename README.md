Favorites Project

Front-end 

on folder favorites-frontend

install dependencies
$ npm install

serve with hot reload at localhost:3000
$ npm run dev

Back-end

on folder favorites

$ docker-compose build
$ docker-compose up -d
$ docker-compose exec server rake db:create db:migrate

to execute test

$ docker-compose exec server rails test

load fixtures and populate with endpoint info

$ docker-compose exec server rails db:fixtures:load dev:populate

To login in system use 
email: teste@abacate.com
password: 123123123

Fell free to create another account


