# frozen_string_literal: true

class CreateBuildings < ActiveRecord::Migration[6.0]
  def change
    create_table :buildings, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.string :title
      t.string :city
      t.string :area
      t.string :state
      t.string :min_area
      t.string :max_area
      t.string :image_url

      t.timestamps
    end
  end
end
