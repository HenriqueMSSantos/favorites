import Jsona from 'jsona'
const dataFormatter = new Jsona()
export default function ({ $axios }) {
  $axios.onRequest((config) => {
    if (config.url[0] === '/') {
      // config.url = 'https://siacad-rs.herokuapp.com' + config.url
      config.url = 'http://localhost:3000' + config.url
      if (config.method === 'post' || config.method === 'patch') {
        config.headers['content-type'] = 'application/json'
        config.headers.accept = 'application/json'
      }
      if (config.method === 'get') {
        config.headers['content-type'] = 'application/vnd.api+json'
        config.headers.accept = 'application/vnd.api+json'
      }
    }
  })
  $axios.onError((err) => {
    console.log(err.response) // eslint-disable-line
  })
  $axios.onResponse((response) => {
    if (response.config.url.includes(process.env.baseUrl)) {
      try {
        return { ...response, json: dataFormatter.deserialize(response.data) }
      } catch {
        return response
      }
    }
    return response
  })
}
