# frozen_string_literal: true

class BuildingsController < ApplicationController
  before_action :authenticate_user, except: [:show, :favorites]

  def show
    render(json: { buildings: Building.all })
  end

  def create_favorite
    user_id = current_user.id
    building_id = params[:building_id]

    UsersBuilding.create(user_id: user_id, building_id: building_id)

    render json: 'ok', status: :created
  end

  def favorites
    @response = FavoritesFinder.new(nil).find(current_user.id)
    render(json: { buildings: @response })
  end

  def delete_favorite
    UsersBuilding.find(params[:id]).delete

    render json: 'ok', status: :created
  end
end
