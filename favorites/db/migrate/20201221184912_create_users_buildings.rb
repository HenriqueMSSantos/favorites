# frozen_string_literal: true

class CreateUsersBuildings < ActiveRecord::Migration[6.0]
  def change
    create_table :users_buildings, id: :uuid, default: 'gen_random_uuid()' do |t|
      t.references :user,
                   type: :uuid,
                   null: false,
                   foreign_key: {
                     on_update: :cascade,
                     on_delete: :cascade
                   }
      t.references :building,
                   type: :uuid,
                   null: false,
                   foreign_key: {
                     on_update: :cascade,
                     on_delete: :cascade
                   }

      t.timestamps
    end
  end
end
