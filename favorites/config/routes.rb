# frozen_string_literal: true

Rails.application.routes.draw do
  get 'buildings', to: 'buildings#show'
  post 'favorite_building', to: 'buildings#create_favorite'
  get 'user_favorites', to: 'buildings#favorites'
  delete 'delete_favorite/:id', to: 'buildings#delete_favorite'


  # devise auth login
  get 'user', to: 'application#user'


  # Routes for divese
  devise_for :users,
             path: '',
             path_names: {
               sign_in: 'login',
               sign_out: 'logout',
               registration: 'signup'
             },
             controllers: {
               sessions: 'sessions',
               registrations: 'registrations'
             }

  # Filter
  post 'people_finder', to: 'filter#people'
  get 'diagnostic_filter', to: 'filter#diagnostic'
  get 'document_finder/:id', to: 'filter#document_finder'
  get 'personal_data/:id', to: 'filter#personal_data'

end
