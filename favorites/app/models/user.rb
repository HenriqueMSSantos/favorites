# frozen_string_literal: true

class User < ApplicationRecord
  include Devise::JWT::RevocationStrategies::JTIMatcher
  has_many :users_buildings, dependent: :destroy
  has_many :buildings, through: :users_buildings

  validates :email,
            uniqueness: { case_sensitive: false },
            presence: true

  validates :password,
            presence: true,
            length: { in: 8..72 }

  devise :registerable,
         :database_authenticatable,
         :jwt_authenticatable,
         jwt_revocation_strategy: self
end
