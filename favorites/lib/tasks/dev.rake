# frozen_string_literal: true

require 'net/http'
namespace :dev do
  desc 'Populate buildings'
  task populate: :environment do
    @url = URI('https://www.orulo.com.br/api/v2/buildings')
    @http = Net::HTTP.new(@url.host, @url.port)
    @http.use_ssl = true
    @http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    @request = Net::HTTP::Get.new(@url)
    auth = 'Bearer ' + Rails.application.credentials.token_api
    @request['content-type'] = 'application/json'
    @request['Authorization'] = auth
    @request['Accept'] = 'application/json'
    @response = @http.request(@request)

    buildings = JSON.parse(@response.body)['buildings']

    create_buildings = []

    buildings.each do |building|
      create_buildings << {
        title: building['name'],
        city: building['address']['city'],
        state: building['address']['state'],
        area: building['address']['area'],
        min_area: building['min_area'],
        max_area: building['max_area'],
        image_url: building['default_image']['520x280'],
        created_at: DateTime.now,
        updated_at: DateTime.now
      }
    end

    Building.insert_all(create_buildings)
  end
end
