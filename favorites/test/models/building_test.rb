# frozen_string_literal: true

require 'test_helper'

class BuildingTest < ActiveSupport::TestCase
  test 'fixture is valid' do
    @objective = buildings(:building_one)
    assert @objective.valid?
  end
end
