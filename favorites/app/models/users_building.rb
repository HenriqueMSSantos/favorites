# frozen_string_literal: true

class UsersBuilding < ApplicationRecord
  belongs_to :user
  belongs_to :building

  validates :user, presence: :true
  validates :building, presence: :true, uniqueness: true
end
